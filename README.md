<h1 align="center">FS-SKELETON</h1>  
<p  align="center">Template generator to build and deploy <i>Flask </i>Applications. </br>
</p>  

## Prerequisites  
**Python 3 & Virtual Environments** 

**Serverless Framework** - [Installation Guide](https://serverless.com/framework/docs/providers/aws/guide/installation/)
## Installation  

**1. Create new python virtual environment**
```
mkvirtualenv fs-skeleton
workon fs-skeleton
```

**2. Install Python dependencies**  
```pip install -r requirements.txt```

## Usage Guide  
_Once the previous steps are completed launch the program using the command below._  
_A prompt will ask you to give a name to your application._

```./fs-skeleton.sh```  

**Note:** _Your new application will be created in your HOME directory._

``` cd ~/yourApplication``` 


## Application Skeleton  

### Installation  
_To see if all this is working enter the following commands_

```
mkvirtualenv your-project-name
workon your-project-name

pip install -r requirements.txt
```

_Now you will need to install serverless plugins needed_

```
npm i serverless-python-requirements

npm i serverless-api-stage

npm install --save serverless-wsgi

```

### Deploy your Application  
_Make sure you have your AWS credentials set before any attempt of deploying_  
[Setup AWS credentials for serverless](https://serverless.com/framework/docs/providers/aws/guide/credentials/)

_Fill the **variables.yml** file with inttest database config_

_Finally run_
```
sls deploy
``` 
### 
## Author
* **Enrique Mendez** - *Initial work* - [Kike Mendez](https://github.com/KikeMendez)

