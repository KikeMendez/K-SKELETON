'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

from config.path import application_paths
from resources.classes.skeleton import Skeleton
from resources.prompt import application_name

def build_skeleton():
    """
    Build skeleton
    """
    return Skeleton.build(application_paths(application_name()))
