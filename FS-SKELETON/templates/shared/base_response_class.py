'''
Author: Enrique Mendez
Created Date: Tuesday, June 5th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template = """'''
Author: Enrique Mendez
Created Date: Tuesday, June 5th 2018
Github: https://github.com/KikeMendez
'''

import json
import jsonschema
import datetime
from flask import Response

class ResponseFormat(object):
    """'''"""
    This class will transform your response into json.
    Also will add headers before send the response out.
    """'''"""

    @classmethod
    def convert_to_json(cls,data,message,status_code):
        result = cls._validate_input_response(data,message,status_code)
        result["body"] = json.dumps(result["body"], default=cls._json_serialize_date, sort_keys=False)
        output = Response(
            response=result["body"],
            status=result["statusCode"],
            content_type="application/json"
            )
        # Adding CORS headers.
        output.headers["Access-Control-Allow-Origin"] = "*"
        output.headers["Access-Control-Allow-Credentials"] = True
        return output

    @classmethod
    def _json_serialize_date(cls,obj):
        """'''"""
        JSON serializer for objects not serializable by default json code.
        Serializing datetime without it will raise exceptions.
        """'''"""
        if isinstance(obj, (datetime.datetime, datetime.date)):
            return obj.isoformat()
        raise TypeError("Type %s not serializable" % type(obj))

    @classmethod
    def _validate_input_response(cls,data,message,status_code):
        try:
            if type(message) != str:
                raise ValueError("Message must be string")
            elif type(status_code) is not int:
                raise ValueError("Status code must be integer")
            else:
                result = {"statusCode": status_code,"body": {"data": {}, "message": message}}
                result["body"]["data"]["items"] = data
                return result
        except Exception as e:
            raise(e)

"""
    return template