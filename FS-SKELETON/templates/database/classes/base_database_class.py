'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template = """import logging

import pymysql.cursors
from pymysql.err import IntegrityError

logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()

class Database(object):

    charset = "utf8"
    sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_DATE,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO"
    mysql_cursor = pymysql.cursors.DictCursor
    timeout =  5

    def __init__(self, host, username, password, db_name, db_port):
        self.host = host
        self.username = username
        self.password = password
        self.db_name = db_name
        self.db_port = int(db_port)

    def connect(self):
        try:
            connection = pymysql.connect(
                host = self.host,
                user = self.username,
                passwd = self.password,
                db = self.db_name,
                sql_mode = self.sql_mode,
                port = self.db_port,
                charset = self.charset,
                connect_timeout = self.timeout,
                cursorclass = self.mysql_cursor
            )
        except Exception as e:
            logger.error("Could not connect to MySql DB: {}".format(e))
            raise
        return connection

"""
    return template

