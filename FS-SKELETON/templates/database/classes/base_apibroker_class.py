'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template = """from .database import Database

class Apibroker(Database):

    def __init__(self, credentials):
        self.username = credentials["apibUsername"]
        self.password = credentials["apibPassword"]
        self.db_name =	credentials["apibName"]
        self.db_port =	credentials["apibPort"]
        self.host =	credentials["apibHost"]
        super().__init__(self.host, self.username, self.password, self.db_name, self.db_port)

    def connect(self):
        return super().connect()

"""
    return template