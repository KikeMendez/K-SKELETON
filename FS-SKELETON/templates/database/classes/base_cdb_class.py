'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """from .database import Database

class Cdb(Database):

    def __init__(self, credentials):
        self.username = credentials["cdbUsername"]
        self.password = credentials["cdbPassword"]
        self.db_name =	credentials["cdbName"]
        self.db_port =	credentials["cdbPort"]
        self.host =	credentials["cdbHost"]
        super().__init__(self.host, self.username, self.password, self.db_name, self.db_port)

    def connect(self):
        return super().connect()
"""
	return template
