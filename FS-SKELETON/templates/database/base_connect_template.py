'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """from .classes.apibroker import Apibroker
from .classes.cdb import Cdb
from .helpers import db_credentials

APIBROKER = "apibroker"
CDB = "cdb"

def connect_to(database):

    database = database.lower()

    database_credentials = db_credentials.get(APIBROKER,CDB)

    if database == APIBROKER:
        if database_credentials is not None:
            return Apibroker(database_credentials[APIBROKER]).connect()

    elif database == CDB:
        if database_credentials is not None:
            return Cdb(database_credentials[CDB]).connect()
    else:
        print("Database name: {} not found".format(database))
        return None

"""
	return template