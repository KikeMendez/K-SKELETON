'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

from flask import request
import json

def get(apibroker_key,cdb_key):
    """'''"""
    Get databases credentials from authorizer
    """'''"""
    if "API_GATEWAY_AUTHORIZER" in request.environ:

        authorizer = request.environ['API_GATEWAY_AUTHORIZER']
        cdb_keys = ["cdbHost","cdbUsername","cdbPassword","cdbName","cdbPort"]
        apibroker_keys = ["apibHost","apibUsername","apibPassword","apibName","apibPort"]

        cdb = {}
        apibroker = {}

        for key, value in authorizer.items():
            if key in cdb_keys:
                cdb[key] = value

            if key in apibroker_keys:
                apibroker[key] = value

        return {
            apibroker_key : apibroker,
            cdb_key : cdb
        }

    else:
        print("API_GATEWAY_AUTHORIZER key is not set")
        return None

"""
	return template