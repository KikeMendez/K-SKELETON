'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """STAGE_VARIABLES:
  apibHost: ''
  apibName: ''
  apibPort: 3306
  apibUsername: ''
  apibPassword: ''
  gateway: 'API'
"""
	return template