'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template ="""from flask import Flask
from handler.apibroker.example import get_domains
from handler.cdb.example import self_pages

app = Flask(__name__)

@app.route("/hello_kike_api", methods=['GET'])
def hello_kike():
    return get_domains()

@app.route("/hello_kike_cdb", methods=['GET'])
def hello_kike_cdb():
    return self_pages()

"""
    return template