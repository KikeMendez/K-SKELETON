'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """service: kiketest

plugins:
  - serverless-python-requirements
  - serverless-api-stage
  - serverless-wsgi


# For serverless-wsgi plugin.
custom:
  stageSettings:
  # CacheClusterEnabled: true
  # CacheClusterSize: '0.5'
    Variables: ${file(./variables.yml):STAGE_VARIABLES}
    MethodSettings:
      LoggingLevel: INFO
      # CachingEnabled: true
      # CacheTtlInSeconds: 3600
  wsgi:
    app: app.app
    packRequirements: false
  pythonRequirements:
    dockerizePip: non-linux

provider:
  name: aws
  runtime: python3.6
  stage: test
  region: eu-west-3
  memorySize: 128
  # vpc: ${file(./variables.yml):STAGE_VARIABLES}


#Headers
httpDefaults: &http_defaults
  method: g
  cors:
    origins:
      - '*'
    headers:
      - X-Forwarded-Host
      - Cookie

# you can add packaging information here
# individually: true - each lambda function has its own package.
package:
  # individually: false
  # include:

# Exclude everything not required in each package to keep their size minimal.
  # exclude:
    - README.md
    - package-lock.json
    - package.json
    - .gitignore
    - .git/**
    - node_modules/**

functions:
  hello_kike:
    handler: wsgi.handler
    events:
      - http:
          path: '/hello_kike_api'
          authorizer:
            arn:  arn:aws:lambda:eu-west-3:708614382518:function:testAuth-test-authorizerFunc
            type: request
            identitySource: method.request.header.X-Forwarded-Host, method.request.header.Cookie # set headers
            resultTtlInSeconds: 0 # caching authorizer
          method: GET
          cors: true

      - http:
          path: '/hello_kike_cdb'
          authorizer:
            arn:  arn:aws:lambda:eu-west-3:708614382518:function:testAuth-test-authorizerFunc
            type: request
            identitySource: method.request.header.X-Forwarded-Host, method.request.header.Cookie # set headers
            resultTtlInSeconds: 0 # caching authorizer
          method: GET
          cors: true
"""
	return template