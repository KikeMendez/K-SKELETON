'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template = """botocore==1.9.19
certifi==2018.1.18
chardet==3.0.4
click==6.6
docutils==0.14
idna==2.6
itsdangerous==0.24
jmespath==0.9.3
jsonschema==2.6.0
MarkupSafe==1.0
PyMySQL==0.8.0
python-dateutil==2.6.1
six==1.11.0
typing==3.5.3.0
urllib3==1.22
requests==2.18.4
Werkzeug==0.14.1
Jinja2==2.10
Flask==0.12.2
"""
    return template