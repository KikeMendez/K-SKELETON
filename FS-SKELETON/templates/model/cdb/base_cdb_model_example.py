'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''
import pymysql

from database.connect import connect_to

def get_self_pages():
    cdb = connect_to("cdb")

    sql = "Select * from self_pages"

    result = {}

    with cdb.cursor() as cursor:

        cursor.execute(sql)

        data = cursor.fetchall()

        result = data

        cdb.close()

    return result
"""
	return template