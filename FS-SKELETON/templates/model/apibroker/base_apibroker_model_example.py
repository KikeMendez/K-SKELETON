'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''
import pymysql

from database.connect import connect_to

def get_domains_by_customer_id(customer_id):
    # Get connection instance
    apibroker = connect_to("apibroker")

    # Prepare query
    sql = "Select Id,Customer_ID,Host,Name,Product from domains where Product in ('FS-SS/self','FS-ASUI') and customer_ID=%(customer_ID)s"

    query_args = {"customer_ID": customer_id }

    result = {}

    with apibroker.cursor() as cursor:

        cursor.execute(sql, query_args)

        data = cursor.fetchall()

        result = data

        apibroker.close()

    return result
"""
	return template