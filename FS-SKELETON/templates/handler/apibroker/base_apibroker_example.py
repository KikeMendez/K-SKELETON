'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''

def get():
	template = """'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''

from flask import request
from shared.response import ResponseFormat
from model.apibroker.example import get_domains_by_customer_id

def get_domains():

    # Get customer id
    customer_ID =  request.environ["API_GATEWAY_AUTHORIZER"]["customerId"]
    domains =  get_domains_by_customer_id(customer_ID)
    return ResponseFormat.convert_to_json(domains,"All domains here",200)

"""
	return template