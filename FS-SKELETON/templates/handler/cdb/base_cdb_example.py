'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''

def get():
    template = """'''
Author: Enrique Mendez
Created Date: Monday, June 4th 2018
Github: https://github.com/KikeMendez
'''

from shared.response import ResponseFormat
from model.cdb.example import get_self_pages

def self_pages():
    self_pages_data = get_self_pages()
    message = "My response class is working :)"
    status_code = 200
    return ResponseFormat.convert_to_json(self_pages_data,message,status_code)

"""
    return template