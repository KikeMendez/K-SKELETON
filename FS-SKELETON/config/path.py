'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

import os

from fabric.colors import red

import templates

def application_paths(application_name):
    """
    Paths config for flask application
    """
    if "HOME" in os.environ:

        # Parent Directory
        APP = os.path.join(os.environ['HOME'], application_name + "/app")

        # Child Directories
        DATABASE = os.path.join(APP, "database")
        MODEL = os.path.join(APP, "model")
        HANDLER = os.path.join(APP, "handler")
        SHARED = os.path.join(APP, "shared")

        # Sub-Child Directories
        DATABASE_CLASSES = os.path.join(DATABASE, "classes")
        DATABASE_HELPERS = os.path.join(DATABASE, "helpers")
        HANDLER_APIBROKER = os.path.join(HANDLER,"apibroker")
        HANDLER_CDB = os.path.join(HANDLER,"cdb")
        MODEL_APIBROKER = os.path.join(MODEL, "apibroker")
        MODEL_CDB = os.path.join(MODEL, "cdb")

        return {
            "directories": {
                "app": APP,
                "database": DATABASE,
                "database_classes": DATABASE_CLASSES,
                "database_helpers": DATABASE_HELPERS,
                "handler": HANDLER,
                "handler_apibroker": HANDLER_APIBROKER,
                "handler_cdb": HANDLER_CDB,
                "model": MODEL,
                "model_cdb": MODEL_CDB,
                "model_apibroker": MODEL_APIBROKER,
                "shared" : SHARED
            },
            "files": {
                "app": {
                    "app": {
                        "path": os.path.join(APP, "app.py"),
                        "template": templates.base_flask_app_template.get()
                    },
                    "serverless": {
                        "path": os.path.join(APP, "serverless.yml"),
                        "template": templates.base_serverless_template.get()
                    },
                    "stage_variables": {
                        "path":  os.path.join(APP, "variables.yml"),
                        "template": templates.base_serverless_variables_template.get()
                    },
                    "requirements": {
                        "path": os.path.join(APP, "requirements.txt"),
                        "template":  templates.python_requirements_template.get()
                    }
                },
                "database": {
                    "init": {
                        "path": os.path.join(DATABASE, "__init__.py"),
                        "template": ""
                    },
                    "connect": {
                        "path": os.path.join(DATABASE, "connect.py"),
                        "template": templates.base_connect_template.get()
                    }
                },
                "database_classes": {
                    "init": {
                        "path": os.path.join(DATABASE_CLASSES,"__init__.py"),
                        "template": ""
                    },
                    "apibroker": {
                        "path": os.path.join(DATABASE_CLASSES,"apibroker.py"),
                        "template": templates.base_apibroker_class.get()
                    },
                    "cdb": {
                        "path": os.path.join(DATABASE_CLASSES,"cdb.py"),
                        "template": templates.base_cdb_class.get()
                    },
                    "database": {
                        "path": os.path.join(DATABASE_CLASSES,"database.py"),
                        "template": templates.base_database_class.get()
                    }
                },
                "database_helpers": {
                    "init": {
                        "path":  os.path.join(DATABASE_HELPERS, "__init__.py"),
                        "template": ""
                    },
                    "db_credentials": {
                        "path":  os.path.join(DATABASE_HELPERS, "db_credentials.py"),
                        "template": templates.base_db_credentials.get()
                    }
                },
                "handler": {
                    "base_init": {
                        "path":  os.path.join(HANDLER,"__init__.py"),
                        "template": ""
                    },
                    "init_apibroker": {
                        "path": os.path.join(HANDLER_APIBROKER,"__init__.py"),
                        "template": ""
                    },
                    "apibroker_example": {
                        "path": os.path.join(HANDLER_APIBROKER,"example.py"),
                        "template": templates.base_apibroker_example.get()
                    },
                    "init_cdb": {
                        "path": os.path.join(HANDLER_CDB,"__init__.py"),
                        "template": ""
                    },
                    "cdb_example": {
                        "path": os.path.join(HANDLER_CDB,"example.py"),
                        "template": templates.base_cdb_example.get()
                    }
                },
                "model": {
                    "base_init": {
                        "path":  os.path.join(MODEL,"__init__.py"),
                        "template": ""
                    },
                    "init_apibroker": {
                        "path": os.path.join(MODEL_APIBROKER,"__init__.py"),
                        "template": ""
                    },
                    "apibroker_example": {
                        "path": os.path.join(MODEL_APIBROKER,"example.py"),
                        "template": templates.base_apibroker_model_example.get()
                    },
                    "init_cdb": {
                        "path": os.path.join(MODEL_CDB,"__init__.py"),
                        "template": ""
                    },
                    "cdb_example": {
                        "path": os.path.join(MODEL_CDB,"example.py"),
                        "template": templates.base_cdb_model_example.get()
                    }
                },
                "shared": {
                    "base_init" : {
                        "path": os.path.join(SHARED,"__init__.py"),
                        "template": ""
                    },
                    "response_class": {
                        "path": os.path.join(SHARED,"response.py"),
                        "template": templates.shared.base_response_class.get()
                    }
                }
            }
        }
    else:
        print(red("HOME PATH NOT FOUND"))
        exit(1)
