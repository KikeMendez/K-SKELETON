'''
Author: Enrique Mendez
Created Date: Wednesday, May 30th 2018
Github: https://github.com/KikeMendez
'''

import os

from fabric.colors import blue,red,white,cyan,yellow,magenta,green

class Skeleton():
    """
    Main class to create templates files for lambda projects
    """
    @classmethod
    def build(cls,paths):
        """
        Build application
        """
        cls._create_directory(paths["directories"])
        cls._create_template(paths["files"])

    @classmethod
    def _create_directory(cls,file):
        try:
            for path in file.values():
                os.makedirs(path)
        except OSError:
            print(yellow("\nFS-skeleton: ") + red("Error, App already exists in location: ") + "{}".format(file["app"]))
            print(yellow("FS-skeleton: ") + white("Running out of ideas? I think you should try something like this... ") + "lambadaSupperCoolApp")
            exit(1)

    @classmethod
    def _create_template(cls,files):
        total_files = 0
        total_file_size = 0
        print(yellow("FS-skeleton: ") + white("Generating templates..",True))
        for content in files.values():
            for file in content.values():
                path = file["path"]
                template = file["template"]
                if not os.path.exists(path):
                    cls._write_template(path,template)
                    total_files = total_files + 1
                    total_file_size = total_file_size + cls._get_file_size(path)
                else:
                    print(red("\nFS-skeleton: {} already exists in this location".format(path)))
                    exit(1)
        cls._skeleton_status(total_files,total_file_size)

    @classmethod
    def _write_template(cls,path,template):
        with open(path,'w') as f:
            f.write(template)
            f.close()
            file_size = cls._get_file_size(path)
            print(yellow("FS-skeleton: ")  + cyan("location: ") + "{} ".format(path) +  cyan(" size: ") + str(file_size) + " bytes")

    @classmethod
    def _get_file_size(cls,path):
        size = os.stat(path).st_size
        return size

    @classmethod
    def _skeleton_status(cls,total_templates, template_size):
        print(yellow("\nFS-Skeleton: ") + cyan("status:", True) + " Done")
        print(yellow("FS-Skeleton: ") + cyan("templates created: ",True) + str(total_templates) + cyan(" total size: ",True) + str(template_size) + " Bytes")