'''
Author: Enrique Mendez
Created Date: Friday, June 1st 2018
Github: https://github.com/KikeMendez
'''

from fabric.colors import white,cyan,yellow,red

def application_name():
    """
    Display prompt to get the app name
    """
    app_name = None
    print(yellow("FS-skeleton: ") + red("(App Name) must be at least 3 characters long", True))
    while not app_name or len(app_name) < 3:
        app_name =  input(yellow("FS-skeleton: ") + white("Give a name to your App: ",True))
    return app_name.strip()
